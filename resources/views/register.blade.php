<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Form SignUp</title>
</head>

<body>
  <h1>Buat Account Baru!</h1>
  <h2>Sign Up Form</h2>
  <form action="/welcome" method="post">
    @csrf
    <label>First name:</label><br /><br />
    <input type="text" name="fname" placeholder="input your first name" /><br /><br />
    <label>Last name:</label><br /><br />
    <input type="text" name="lname" placeholder="input your last name" /><br /><br />
    <label>Gender:</label><br /><br />
    <input type="radio" name="Male" />
    <label>Male</label><br />
    <input type="radio" name="Female" />
    <label>Female</label><br />
    <input type="radio" name="Other" />
    <label>Other</label><br /><br />
    <label>Nationality:</label><br /><br />
    <select>
      <option>Indonesia</option>
      <option>Malaysia</option>
      <option>Singapura</option>
    </select>
    <br /><br />
    <label>Language Spoken:</label><br /><br />
    <input type="checkbox" name="Bahasa Indonesia" />
    <label>Bahasa Indonesia</label><br />
    <input type="checkbox" name="English" />
    <label>English</label><br />
    <input type="checkbox" name="Other" />
    <label>Other</label><br /><br />
    <label>Bio:</label><br /><br />
    <textarea cols="30" rows="10"></textarea><br /><br />
    <button type="submit" value="welcome">Sign Up</button>
  </form>
</body>

</html>